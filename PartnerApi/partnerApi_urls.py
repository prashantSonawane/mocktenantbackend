from django.urls import path

from .views import Partner_UserView, Partner_UserLoginView, checkUserAvailabilty

app_name = 'partner'

urlpatterns = [
    path('register/', Partner_UserView.as_view()),
    path('getUserDetails/',Partner_UserLoginView.as_view()),
    path('login/', Partner_UserLoginView.as_view()),
    path('checkUserAvailablity/', checkUserAvailabilty.as_view())
]
