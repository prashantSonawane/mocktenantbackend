from rest_framework import serializers

from UserApi.models import Users


class PartnerUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('fullname', 'firstName', 'lastName', 'emailId', 'DOB', 'country', 'password', 'mobileNo', 'ownerId')
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        return Users.objects.create(**validated_data)
