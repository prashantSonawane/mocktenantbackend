import base64;
from random import randint

from django.utils.crypto import get_random_string
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from UserApi.models import Users
from .partnerApi_serializers import PartnerUserSerializer


# Create your views here.

# Partner Registarion API
class Partner_UserView(APIView):
    # Partner Add User
    def post(self, request):
        user = request.data
        requestId = user['requestId']
        ipAddress = user['ipAddress']
        channelId = user['channelId']
        partnerId = user['partnerId']
        user['fullname'] = user['firstName'] + ' ' + user['lastName']
        password = user['password']
        decodePassword = base64.b64decode(password)
        password = decodePassword.decode('utf-8')
        user['password'] = password
        serializer = PartnerUserSerializer(data=user, many=False)
        if serializer.is_valid(raise_exception=True):
            user_saved = serializer.save(ownerId=randint(100000, 999999))
        return Response({
            "message": "Successful",
            "loginId": user_saved.emailId,
            "responseId": randint(10000, 99999),
            "sessionId": get_random_string(32),
            "requestId": "MOCK" + str(randint(1000000, 9999999)),
            "ownerId": user_saved.ownerId,
            "partnerId": "MOCK",
            "responseFlag": "S",
            "responseName": "MOCKREGISTRATION"
        }, status=status.HTTP_200_OK)


# Partner Login
class Partner_UserLoginView(APIView):
    def post(self, request):
        user = request.data
        requestId = user['requestId']
        ipAddress = user['ipAddress']
        channelId = user['channelId']
        partnerId = user['partnerId']

        # Login
        if (user['requestName'] == 'LOGIN'):
            password = user['password']
            decodePassword = base64.b64decode(password)
            password = decodePassword.decode('utf-8')
            try:
                get_user = Users.objects.get(emailId=user['loginId'], password=password)
                serializer = PartnerUserSerializer(get_user, many=False)
                return Response({
                    "lastname": get_user.lastName,
                    "loginid": get_user.emailId,
                    "isLeadOTPVerified": "N",
                    "mobileOTPVerificationRequired": "N",
                    "responseName": "LOGIN",
                    "sessionId": get_random_string(32),
                    "ownerId": get_user.ownerId,
                    "responseFlag": "S",
                    "firstName": get_user.firstName,
                    "lastLoginTime": "29-APR-20 12:25:17",
                    "forceChangeFlag": False,
                    "customerType": "Normal",
                    "requestId": requestId,
                    "isMPINCreated": "N",
                    "partnerId": "MOCK",
                    "status": "ACTIVE",
                    "responseId": requestId
                }, status=status.HTTP_200_OK)
            except:
                return Response({
                    "responseStatus": 404,
                    "message": "Invalid Username or Password",
                    "status": "FAILURE",
                    "responseFlag": "F",
                }, status=status.HTTP_404_NOT_FOUND)
        # Get User Details
        elif (user['requestName'] == 'GETUSERDETAILS'):
            try:
                get_user = Users.objects.get(ownerId=user['ownerId'])
                serializer = PartnerUserSerializer(get_user, many=False)
                return Response({
                    "loginId": get_user.emailId,
                    "lastName": get_user.lastName,
                    "occupation": "",
                    "offPhone": "1-023-77777777",
                    "requestId": requestId,
                    "ownerId": get_user.ownerId,
                    "state": "California",
                    "partnerId": "MOCK",
                    "country": get_user.country,
                    "city": "test city",
                    "message": "Success",
                    "pinCode": "12365",
                    "responseId": requestId,
                    "emailId": get_user.emailId,
                    "nationality": "",
                    "alternateNo": "",
                    "sessionId": get_random_string(32),
                    "address": "45~~testet~F~F~F",
                    "dob": "16-MAR-1990",
                    "responseFlag": "S",
                    "firstName": get_user.firstName,
                    "referenceNo": "",
                    "mobileNo": get_user.mobileNo,
                    "responseName": "GETUSERDETAILS"
                }, status=status.HTTP_200_OK)
            except:
                return Response({
                    "responseStatus": 404,
                    "message": "User Not Found",
                    "status": "FAILURE",
                    "responseFlag": "F",
                }, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response({
                "responseStatus": 400,
                "message": "Request name not found",
                "status": "FAILURE",
                "responseFlag": "F",
            }, status=status.HTTP_400_BAD_REQUEST)


# checkUserAvail
class checkUserAvailabilty(APIView):
    def post(self, request):
        user = request.data
        try:
            get_user = Users.objects.get(emailId=user['loginId'])
            serializer = PartnerUserSerializer(get_user, many=False)
            return Response({
                "responseStatus": 200,
                "requestId": user['requestId'],
                "responseName": user['requestName'],
                "message": "This Email Address cannot be used",
                "responseId": user['requestId'],
                "partnerId": "MOCK",
                "responseFlag": "F",
            }, status=status.HTTP_200_OK)
        except:
            return Response({
                "responseStatus": 200,
                "requestId": user['requestId'],
                "responseName": user['requestName'],
                "message": "This Email Address is available for use",
                "responseId": user['requestId'],
                "partnerId": "MOCK",
                "responseFlag": "S",
            }, status=status.HTTP_200_OK)
