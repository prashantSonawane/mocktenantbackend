from django.urls import path

from .views import RSSOView, RSSOLoginView

app_name = "SSO"
urlpatterns = [
    path('ReverseSSORequestHandler/', RSSOView.as_view()),
    path('getChecksum/', RSSOView.as_view()),
    path('login/', RSSOLoginView.as_view())
]
