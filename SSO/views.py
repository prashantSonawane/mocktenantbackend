import hashlib;
from random import randint

from django.utils.crypto import get_random_string
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from UserApi.models import Users
from UserApi.serializers import UserSerializer


class RSSOView(APIView):
    def post(self, request):
        request = request.data
        if request['requestName'] == 'GETREVERSESSOTOKEN':
            ownerId = request['ownerId']
            sourceSessionId = request['sourceSessionId']
            sourceChecksum = request['checksum']

            # CREATE CHECKSUM
            salt = "hCgHFY8D99nnkQqwIUvkCNXd8xbyoJyeHRNOJy0Gq/Q="
            checksumString = bytes(
                request['requestName'] + request['sourceUserName'] + request['sourcePassword'] + request[
                    'sourceSessionId'] + str(request['ownerId']) + request['activityCode'] + salt, encoding="utf-8");
            destinationChecksum = hashlib.sha256(checksumString).hexdigest()
            if (sourceChecksum == destinationChecksum):
                try:
                    get_user = Users.objects.get(ownerId=ownerId)
                    accesstoken = randint(100000000, 999999999)
                    get_user.accessToken = accesstoken
                    get_user.save()
                    return Response({
                        "accessUrl": "http://localhost:4200/#/ReverseSSOLogin",
                        "sourceSessionId": sourceSessionId,
                        "accessToken": accesstoken
                    }, status=status.HTTP_200_OK)
                except:
                    return Response({
                        "responseStatus": 404,
                        "responseMessage": "User Not Found,Invalid OwnerId"
                    }, status=status.HTTP_404_NOT_FOUND)
            else:
                return Response({
                    "responseStatus": 400,
                    "responseMessage": "Invalid Data Recieved"
                }, status=status.HTTP_400_BAD_REQUEST)
        elif request['requestName'] == 'SSOAPI':
            salt = "hCgHFY8D99nnkQqwIUvkCNXd8xbyoJyeHRNOJy0Gq/Q="
            checksumString = bytes(
                request['requestName'] + request['sourceUserName'] + request['sourcePassword'] + request[
                    'sourceSessionId'] + str(request['ownerId']) + request['activityCode'] + salt, encoding="utf-8");
            checksum = hashlib.sha256(checksumString).hexdigest()
            return Response({
                "checksum": checksum
            }, status=status.HTTP_200_OK)
        else:
            return Response({
                "responseStatus": 400,
                "responseMessage": "Invalid Request Recieved"
            }, status=status.HTTP_400_BAD_REQUEST)


# Login RSSO
class RSSOLoginView(APIView):
    def post(self, request):
        user_request = request.data
        try:
            get_user = Users.objects.get(ownerId=user_request['ownerId'], accessToken=user_request['accessToken'])
            get_user.accessToken = 0
            get_user.save()
            serializer = UserSerializer(get_user, many=False)
            return Response({"responseStatus": 200, "token": get_random_string(32), "userData": serializer.data},
                            status=status.HTTP_200_OK)
        except:
            return Response({"responseStatus": 404, "responseMessage": "Invalid Data"},
                            status=status.HTTP_404_NOT_FOUND)
