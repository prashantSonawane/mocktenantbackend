# Generated by Django 3.0.5 on 2020-04-22 09:25

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('UserApi', '0007_auto_20200422_1451'),
    ]

    operations = [
        migrations.AlterField(
            model_name='users',
            name='noOfImages',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0),
                                                             django.core.validators.MaxValueValidator(10)]),
        ),
    ]
