# Generated by Django 3.0.5 on 2020-04-16 07:59

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('UserApi', '0003_users_ownerid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='users',
            name='emailId',
            field=models.EmailField(max_length=254, unique=True),
        ),
    ]
