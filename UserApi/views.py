from datetime import datetime
from random import randint

from django.utils.crypto import get_random_string
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Users, BuyImage
from .serializers import UserSerializer, BuyImageSerializer


# Create your views here.

# Mock Registarion API
class UserView(APIView):
    # Tenant User Get List
    def get(self, request):
        users = Users.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response({"responseStatus": 200, "users": serializer.data}, status=status.HTTP_200_OK)

    # Tenant Add User
    def post(self, request):
        user = request.data
        requestId = user['requestId']
        print(requestId)
        firstName, lastName = user['fullname'].split(' ')
        user['firstName'] = firstName
        user['lastName'] = lastName
        serializer = UserSerializer(data=user, many=False)
        if serializer.is_valid(raise_exception=True):
            user_saved = serializer.save(ownerId=randint(100000, 999999))
        return Response({
            "responseStatus": 200,
            "responseMessage": "User Registered Successfully",
            "user_details": [
                {
                    "FullName": user_saved.fullname,
                    "user_EmailId": user_saved.emailId,
                    "OwnerId": user_saved.ownerId
                }
            ]
        }, status=status.HTTP_200_OK)


# Tenant Login
class LoginView(APIView):
    def post(self, request):
        user_request = request.data
        requestId = user_request['requestId']
        print(requestId)
        try:
            get_user = Users.objects.get(country=user_request['country'], emailId=user_request['emailId'],
                                         password=user_request['password'])
            serializer = UserSerializer(get_user, many=False)
            return Response({"responseStatus": 200, "token": get_random_string(32), "userData": serializer.data},
                            status=status.HTTP_200_OK)
        except:
            return Response({"responseStatus": 404, "responseMessage": "Invalid Login emailId or password"},
                            status=status.HTTP_404_NOT_FOUND)


# BuyIMAGE
class BuyImageView(APIView):
    def post(self, request):
        # GET TRANSACTION HISTORY FOR SINGLE USER
        if request.data['requestType'] == 'getTransactionHistory':
            if 'emailId' in request.data:
                user = request.data
                try:
                    get_user = BuyImage.objects.filter(userEmailId=user['emailId'])
                    if (len(get_user) > 0):
                        serializer = BuyImageSerializer(get_user, many=True)
                        return Response({"responseStatus": 200, "userDetails": serializer.data},
                                        status=status.HTTP_200_OK)
                    else:
                        return Response({"responseStatus": 404, "responseMessage": "Not Found"},
                                        status=status.HTTP_404_NOT_FOUND)
                except:
                    return Response({"responseStatus": 404, "responseMessage": "Not Found"},
                                    status=status.HTTP_404_NOT_FOUND)
            else:
                return Response({"responseStatus": 400, "responseMessage": "Must provide EmailId"},
                                status=status.HTTP_400_BAD_REQUEST)
        # BUY IMAGE
        elif request.data['requestType'] == 'buyImages':
            if 'emailId' and 'imageCount' in request.data:
                user = request.data
                get_user = Users.objects.get(emailId=user['emailId'])
                print(user['emailId'])
                create_user = BuyImage.objects.create(user=get_user, userEmailId=get_user.emailId,
                                                      noOfImagesPurchased=user['imageCount'],
                                                      dateOfPurchase=datetime.now(), imagesBuyCount=1,
                                                      transactionAmount=user['transactionAmount'])
                serializer = BuyImageSerializer(create_user, many=False)
                return Response({"responseStatus": 200, "responseMessage": "Transaction Successfull",
                                 "TrasactionData": serializer.data},
                                status=status.HTTP_200_OK)
            else:
                return Response({"responseStatus": 400, "responseMessage": "Must provide EmailId and ImageCount"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"responseStatus": 400, "responseMessage": "Must provide request type"},
                            status=status.HTTP_400_BAD_REQUEST)
