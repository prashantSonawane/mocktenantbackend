from rest_framework import serializers

from .models import Users, BuyImage


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = (
            'fullname', 'firstName', 'lastName', 'emailId', 'DOB', 'country', 'password', 'mobileNo', 'ownerId',
            'noOfImagePurchased', 'isPrime')
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        return Users.objects.create(**validated_data)


class BuyImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuyImage
        fields = (
        'id', 'user', 'userEmailId', 'noOfImagesPurchased', 'dateOfPurchase', 'imagesBuyCount', 'transactionAmount')
