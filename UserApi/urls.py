from django.urls import path

from .views import UserView, LoginView, BuyImageView

app_name = "users"
urlpatterns = [
    path('register/', UserView.as_view()),
    path('login/', LoginView.as_view()),
    path('buyImages/', BuyImageView.as_view()),
    path('getTransactionHistory/', BuyImageView.as_view())
]
