from datetime import datetime

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


# Create your models here.
class Users(models.Model):
    fullname = models.CharField(max_length=30, blank=True)
    firstName = models.CharField(max_length=30, blank=True)
    lastName = models.CharField(max_length=30, blank=True)
    emailId = models.EmailField(unique=True, blank=False)
    DOB = models.DateField(blank=False)
    country = models.CharField(max_length=30, blank=False)
    password = models.CharField(max_length=30, blank=False)
    mobileNo = models.CharField(max_length=10, blank=False)
    ownerId = models.IntegerField(blank=True, null=True, default=0)
    isPrime = models.BooleanField(default=False, blank=True, null=True)
    accessToken = models.IntegerField(blank=True, null=True, default=0)
    def noOfImagePurchased(self):
        image = BuyImage.objects.filter(user=self)
        for i in image:
            if i.noOfImagesPurchased == 0:
                return 0;
            else:
                return i.noOfImagesPurchased


class BuyImage(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    userEmailId = models.EmailField(blank=False, null=True)
    noOfImagesPurchased = models.IntegerField(default=0, validators=(MinValueValidator(1), MaxValueValidator(5)))
    dateOfPurchase = models.DateTimeField(default=datetime.now, blank=True)
    imagesBuyCount = models.IntegerField(default=0, blank=True, null=True)
    transactionAmount = models.IntegerField(default=0, blank=True, null=True)
